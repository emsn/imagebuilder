#!/bin/bash

mkdir build
cd build

wget https://github.com/hypriot/image-builder-rpi/archive/v1.10.0-rc1.tar.gz
tar xvzf v1.10.0-rc1.tar.gz
rm v1.10.0-rc1.tar.gz

cp -r ../files image-builder-rpi-1.10.0-rc1/builder/

cd image-builder-rpi-1.10.0-rc1
make sd-image

mv hypriotos-rpi-dirty.img.zip        ../../emsn.img.zip
mv hypriotos-rpi-dirty.img.zip.sha256 ../../emsn.img.zip.sha256

cd ../..
rm -rf build
