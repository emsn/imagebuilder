#!/bin/bash
/usr/bin/docker pull ansi/emsn:latest
/usr/bin/docker pull ansi/emsn-transmitter:latest
/bin/mkdir /home/pirate/emsn
/usr/bin/docker run -it -d --name emsn-recorder --restart=unless-stopped --rm                                           -v /home/pirate/emsn:/host ansi/emsn-transmitter:latest
/usr/bin/docker run -it -d --name emsn-recorder --restart=unless-stopped --rm --privileged -v /dev/bus/usb:/dev/bus/usb -v /home/pirate/emsn:/host ansi/emsn:latest
